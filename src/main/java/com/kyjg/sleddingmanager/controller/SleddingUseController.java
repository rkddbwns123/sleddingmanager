package com.kyjg.sleddingmanager.controller;

import com.kyjg.sleddingmanager.model.SleddingUseRequest;
import com.kyjg.sleddingmanager.service.SleddingUseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/sledding-use")
public class SleddingUseController {
    private final SleddingUseService sleddingUseService;
    @PostMapping("/data")
    public String setSleddingUse(@RequestBody @Valid SleddingUseRequest request) {
        sleddingUseService.setSleddingUse(request);

        return "OK";
    }
}
