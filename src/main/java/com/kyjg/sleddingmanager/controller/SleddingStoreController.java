package com.kyjg.sleddingmanager.controller;

import com.kyjg.sleddingmanager.model.SleddingStoreItem;
import com.kyjg.sleddingmanager.model.SleddingStorePriceUpdateRequest;
import com.kyjg.sleddingmanager.model.SleddingStoreRequest;
import com.kyjg.sleddingmanager.service.SleddingStoreService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/sledding-store")
public class SleddingStoreController {
    private final SleddingStoreService sleddingStoreService;
    @PostMapping("/data")
    public String setSleddingStore(@RequestBody @Valid SleddingStoreRequest request) {
        sleddingStoreService.setSleddingStore(request);

        return "OK";
    }
    @GetMapping("/all")
    public List<SleddingStoreItem> getSleddingStore() {
        List<SleddingStoreItem> result = sleddingStoreService.getSleddingStore();

        return result;
    }
    @PutMapping("/update/id/{id}")
    public String putSleddingStorePrice(@PathVariable long id,
                                        @RequestBody @Valid SleddingStorePriceUpdateRequest request) {
        sleddingStoreService.putSleddingStorePrice(id, request);

        return "OK";
    }
    @DeleteMapping("/delete-data/id/{id}")
    public String delSleddingStore(@PathVariable long id) {
        sleddingStoreService.delSleddingStore(id);

        return "OK";
    }
}
