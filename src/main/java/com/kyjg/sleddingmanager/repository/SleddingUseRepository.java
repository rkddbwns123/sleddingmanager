package com.kyjg.sleddingmanager.repository;

import com.kyjg.sleddingmanager.entity.SleddingUse;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SleddingUseRepository extends JpaRepository<SleddingUse, Long> {
}
