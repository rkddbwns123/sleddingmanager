package com.kyjg.sleddingmanager.repository;

import com.kyjg.sleddingmanager.entity.SleddingStore;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SleddingStoreRepository extends JpaRepository<SleddingStore, Long> {
}
