package com.kyjg.sleddingmanager.service;

import com.kyjg.sleddingmanager.entity.SleddingStore;
import com.kyjg.sleddingmanager.model.SleddingStoreItem;
import com.kyjg.sleddingmanager.model.SleddingStorePriceUpdateRequest;
import com.kyjg.sleddingmanager.model.SleddingStoreRequest;
import com.kyjg.sleddingmanager.repository.SleddingStoreRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SleddingStoreService {
    private final SleddingStoreRepository sleddingStoreRepository;

    public void setSleddingStore(SleddingStoreRequest request) {
        SleddingStore addData = new SleddingStore();

        addData.setProductName(request.getProductName());
        addData.setSellingPrice(request.getSellingPrice());
        addData.setCotsPrice(request.getCotsPrice());
        addData.setSalesVolume(request.getSalesVolume());

        int profit = request.getSellingPrice() - request.getCotsPrice();

        addData.setNetProfit(profit);
        addData.setTotalSales(request.getSalesVolume() * profit);

        sleddingStoreRepository.save(addData);
    }

    public List<SleddingStoreItem> getSleddingStore() {
        List<SleddingStore> originList = sleddingStoreRepository.findAll();

        List<SleddingStoreItem> result = new LinkedList<>();

        for (SleddingStore item : originList) {
            SleddingStoreItem addItem = new SleddingStoreItem();

            addItem.setId(item.getId());
            addItem.setProductName(item.getProductName());
            addItem.setSellingPrice(item.getSellingPrice());
            addItem.setCotsPrice(item.getCotsPrice());
            addItem.setSalesVolume(item.getSalesVolume());
            addItem.setTotalSales(item.getTotalSales());

            result.add(addItem);
        }

        return result;
    }

    public void putSleddingStorePrice(long id, SleddingStorePriceUpdateRequest request) {
        SleddingStore originData = sleddingStoreRepository.findById(id).orElseThrow();

        originData.setSellingPrice(request.getSellingPrice());
        originData.setCotsPrice(request.getCotsPrice());
        originData.setSalesVolume(request.getSalesVolume());

        int profit = request.getSellingPrice() - request.getCotsPrice();

        originData.setNetProfit(profit);
        originData.setTotalSales(request.getSalesVolume() * profit);

        sleddingStoreRepository.save(originData);
    }
    public void delSleddingStore(long id) {
        sleddingStoreRepository.deleteById(id);
    }
}
