package com.kyjg.sleddingmanager.service;

import com.kyjg.sleddingmanager.entity.SleddingUse;
import com.kyjg.sleddingmanager.enums.UseTIme;
import com.kyjg.sleddingmanager.model.SleddingUseItem;
import com.kyjg.sleddingmanager.model.SleddingUseRequest;
import com.kyjg.sleddingmanager.repository.SleddingUseRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SleddingUseService {
    private final SleddingUseRepository sleddingUseRepository;

    public void setSleddingUse(SleddingUseRequest request) {
        SleddingUse addData = new SleddingUse();

        addData.setCustomerName(request.getCustomerName());
        addData.setCustomerPhone(request.getCustomerPhone());
        addData.setCustomerAge(request.getCustomerAge());
        addData.setUseTime(UseTIme.DAY_TIME);
        if (LocalTime.now().isBefore(LocalTime.of(14, 00))) {
            addData.setUseTime(UseTIme.MORNING_TIME);
        }
        addData.setUsingPrice(4000);
        if (request.getCustomerAge() <= 19 && request.getCustomerAge() >= 8) {
            addData.setUsingPrice(5000);
        } else if (request.getCustomerAge() >= 20) {
            addData.setUsingPrice(7000);
        }
        addData.setIsRideSledding(true);
        if (request.getCustomerAge() >= 65 || request.getCustomerAge() <= 3) {
            addData.setIsRideSledding(false);
        }

        sleddingUseRepository.save(addData);
    }
}
