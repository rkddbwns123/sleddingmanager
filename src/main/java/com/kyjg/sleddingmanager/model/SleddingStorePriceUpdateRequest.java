package com.kyjg.sleddingmanager.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class SleddingStorePriceUpdateRequest {
    @NotNull
    @Min(value = 100)
    @Max(value = 30000)
    private Integer sellingPrice;
    @NotNull
    @Min(value = 100)
    @Max(value = 30000)
    private Integer cotsPrice;
    @NotNull
    @Min(value = 0)
    private Integer salesVolume;
}
