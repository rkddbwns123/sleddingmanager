package com.kyjg.sleddingmanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class SleddingStoreRequest {
    @NotNull
    @Length(min = 1, max = 20)
    private String productName;
    @NotNull
    @Min(value = 100)
    @Max(value = 30000)
    private Integer sellingPrice;
    @NotNull
    @Min(value = 100)
    @Max(value = 30000)
    private Integer cotsPrice;
    @NotNull
    @Min(value = 0)
    private Integer salesVolume;
}
