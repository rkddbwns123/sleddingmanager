package com.kyjg.sleddingmanager.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SleddingStoreItem {
    private Long id;

    private String productName;

    private Integer sellingPrice;

    private Integer cotsPrice;

    private Integer salesVolume;

    private Integer totalSales;
}
