package com.kyjg.sleddingmanager.model;

import com.kyjg.sleddingmanager.enums.UseTIme;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Getter
@Setter
public class SleddingUseItem {

    private Long id;

    private String customerName;

    private String customerPhone;

    private Integer customerAge;
    @Enumerated(value = EnumType.STRING)
    private UseTIme useTime;
}
