package com.kyjg.sleddingmanager.model;

import com.kyjg.sleddingmanager.enums.UseTIme;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class SleddingUseRequest {
    @NotNull
    @Length(min = 1, max = 20)
    private String customerName;
    @NotNull
    @Length(min = 11, max = 20)
    private String customerPhone;
    @NotNull
    @Min(value = 1)
    @Max(value = 100)
    private Integer customerAge;
}
