package com.kyjg.sleddingmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SleddingManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SleddingManagerApplication.class, args);
    }

}
