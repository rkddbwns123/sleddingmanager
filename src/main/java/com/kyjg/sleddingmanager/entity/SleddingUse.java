package com.kyjg.sleddingmanager.entity;

import com.kyjg.sleddingmanager.enums.UseTIme;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalTime;

@Entity
@Getter
@Setter
public class SleddingUse {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 20)
    private String customerName;
    @Column(nullable = false, length = 20)
    private String customerPhone;
    @Column(nullable = false)
    private Integer customerAge;
    @Column(nullable = false, length = 50)
    @Enumerated(value = EnumType.STRING)
    private UseTIme useTime;
    @Column(nullable = false)
    private Integer usingPrice;
    @Column(nullable = false)
    private Boolean isRideSledding;
}
