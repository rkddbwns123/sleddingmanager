package com.kyjg.sleddingmanager.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class SleddingStore {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 20)
    private String productName;
    @Column(nullable = false)
    private Integer sellingPrice;
    @Column(nullable = false)
    private Integer cotsPrice;
    @Column(nullable = false)
    private Integer salesVolume;
    @Column(nullable = false)
    private Integer netProfit;
    @Column(nullable = false)
    private Integer totalSales;
}
