package com.kyjg.sleddingmanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum UseTIme {
    MORNING_TIME("10:00 ~ 13:00"),
    DAY_TIME("14:00 ~ 17:00");

        private final String useTimeName;
}
